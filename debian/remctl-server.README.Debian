remctl-server for Debian
------------------------

This package contains only the daemon (remctld).  For the client, install
remctl-client.

remctld is configured to run by default out of inetd under TCP wrappers so
that you can further restrict access via /etc/hosts.allow and
/etc/hosts.deny.  If you plan on running remctld in stand-alone mode or
under some other network server, comment out the invocation in
/etc/inetd.conf and it won't be readded by upgrades of this package.

Example systemd unit files to start remctld with socket activation are
included in /usr/share/doc/remctl-server/examples.  To start remctld this
way on a system using systemd, first disable running it through inetd as
described above, and then:

    cp /usr/share/doc/remctl-server/examples/remctld.{service,socket} \
        /etc/systemd/system
    systemctl daemon-reload
    systemctl enable remctld
    systemctl start remctld.socket

A sample upstart configuration is included in the same directory.  To use
it, similarly disable running remctld through inetd and then copy it to
/etc/init/remctl.conf.

You don't have to run remctld as root, but of course will need to do so if
you want it to run any programs that need root privileges or want to run
programs that change to various different user identities.  By default it
is set up to run as root.

The default configuration file is /etc/remctl/remctl.conf, but I recommend
that you leave that file unchanged and drop your configuration into
/etc/remctl/conf.d as separate files.  Make sure the name of the file
doesn't contain a period; those are skipped.  See the comments in
/etc/remctl/remctl.conf for more details.

The remctl protocol documentation is included in the remctl-client
package.  To see it, install remctl-client and look in
/usr/share/doc/remctl-client.

 -- Russ Allbery <rra@debian.org>, Mon,  6 Jan 2014 12:58:25 -0800
